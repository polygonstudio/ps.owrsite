# -*- coding: utf-8 -*-
# =============================================================================
#  AUTHOR: Michael Graf <office@polygonstudio.at>
#  ORGANIZATION: polygonstudio.at <http://polygonstudio.at>
#  VERSION: 0.1
#  CREATED: 06-19-2015
#  REVISION: ---
# =============================================================================
from setuptools import setup
from setuptools import find_packages
import os
import subprocess


def get_git_revision_short_hash():
    try:
        return subprocess.check_output(['git',
                                       'rev-parse', '--short', 'HEAD']
                                       ).strip()
    except:
        return

# version = '0.1-' + get_git_revision_short_hash().strip()
version = '0.1'

setup(name='ps.owrsite',
      version=version,
      description="",
      long_description=open("README.md").read() + "\n" +
      open(os.path.join("docs", "HISTORY.txt")).read(),
      # Get more strings from
      # http://www.python.org/pypi?%3Aaction=list_classifiers
      classifiers=[
        "Framework :: Plone",
        "Programming Language :: Python",
        "Topic :: Software Development :: Libraries :: Python Modules",
        ],
      keywords='',
      author='Michael Graf',
      author_email='office@polygonstudio.at',
      license='GPL',
      namespace_packages=['ps'],
      include_package_data=True,
      zip_safe=False,
      packages=find_packages('src'),
      package_dir={'': 'src'},
      install_requires=[
          'setuptools',
          'Plone',
          'Pillow',
          'Products.ClockServer',
          'collective.folderishtraverse',
          'html2text',
          'plone.api',
          'plone.app.contenttypes',
          'plone.app.theming',
          'plone.app.themingplugins',
          'plone.app.dexterity [grok]',
          'plone.namedfile [blobs]',
          'plonetheme.owr'
          ],
      entry_points="""
      [z3c.autoinclude.plugin]
      target = plone
      """,
      )

