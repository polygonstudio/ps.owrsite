# Very simple JSON import of a folder on plone site
#
# Assumptions:
#   - set-site.py
#   - login-as-admin.py
#   - Archetypes
from zope.component import getUtility
from Products.CMFCore.interfaces import ISiteRoot
from zope.component import getUtility
import urllib2
import json
from zope.container.interfaces import INameChooser
from dateutil.parser import parse
from plone.namedfile.file import NamedBlobImage
# from base64 import b64decode
from plone.app.textfield.value import RichTextValue
from plone import api
from plone.dexterity.browser.view import DefaultView
from Products.CMFPlone.utils import _createObjectByType
from plone.i18n.normalizer.interfaces import IIDNormalizer
import transaction
import html2text
import dateutil.parser
from plone.dexterity.utils import datify
from Products.CMFPlone.utils import safe_unicode
import posixpath
import urlparse


class Import(DefaultView):

    """"""

    def __init__(self, context, request):
        self.context = context
        self.request = request

    def __call__(self):
        # return 'foo'
        normalizer = getUtility(IIDNormalizer)
        response = urllib2.urlopen(
            'http://localhost:8080/owr/++resource++ps.owrsite/import.json')
        data = json.load(response)
        nav_root = api.portal.get_navigation_root(self.context)
        folder = '/'.join(nav_root.getPhysicalPath())
        folder = nav_root.restrictedTraverse('/owr/news')
        count = 0

        for d in data['RECORDS']:
            id = normalizer.normalize(d['title'])
            if count <= 1000:
                if id not in folder.objectIds():
                    try:
                        obj = _createObjectByType("News Item", folder, id)
                        obj.title = d['title']
                        try:
                            obj.text = RichTextValue(
                                d['body_value'].decode('utf-8'))
                            # obj.text = RichTextValue(raw=d['body_value'].decode('utf-8'), outputMimeType='text/html')
                        except:
                            obj.text = RichTextValue(
                                str(d['body_value'].encode(encoding='utf-8', errors='ignore')))
                        obj.description = html2text.html2text(
                            d['body_summary'])
                        obj.effective_date = datify(
                            dateutil.parser.parse(d['date_value']))
                        obj.creation_date = datify(
                            dateutil.parser.parse(d['date_value']))
                        images = d['uri'].split(', ')
                        if len(images) >= 1:
                            first_image, other_images = images[0], images[1:]
                            if other_images:
                                for i in other_images:
                                    path = urlparse.urlsplit(i).path
                                    filename = safe_unicode(
                                        posixpath.basename(path))
                                    filename = normalizer.normalize(filename)
                                    subimg = _createObjectByType(
                                        'Image', obj, filename)
                                    subimg.image = self.importImage(i)
                            obj.image = self.importImage(first_image)
                        obj.reindexObject()
                        transaction.get().commit()
                        count += 1
                    except UnicodeEncodeError:
                        pass
                        # print 'Error {0}'.format(str(d['uri']))
                else:
                    print 'id vorhanden {0}'.format(id)
        return 'erstellte Objekte: {0}'.format(count)

    def importImage(self, url=None):
        if url:
            try:
                url = url.replace(' ', '%20')
                req = urllib2.Request(url)
                f = urllib2.urlopen(req, timeout=10)
            except urllib2.HTTPError:
                print 'Could not download image {0}'.format(url)
            else:
                path = urlparse.urlsplit(url).path
                filename = safe_unicode(posixpath.basename(path))
                # print filename
                # image_file = os.path.join(os.path.dirname(__file__), u'image.jpg')
                # filename = safe_unicode(item.filename)
                # namechooser = INameChooser(self.context)
                # id_name = namechooser.chooseName(title, self.context)
                content_type = f.headers.get('Content-Type')
                # Create holder for the image
                data = f.read()
                # im = cStringIO.StringIO(f.read())
                # Open the image with PIL
                # image = Image.open(im)
                # Get its width and height
                # width, height = image.size
                # print image.size
                # print content_type
                # wrapped_data = NamedBlobImage(data=open(image_file, 'r').read(), contentType=content_type, filename=filename)
                wrapped_data = NamedBlobImage(
                    data=data, contentType=content_type, filename=filename)
                # Create content
                # self.context.invokeFactory(portal_type,
                #                           id=id_name,
                #                           title=title,
                #                           description=description[0])
                #newfile = self.context[id_name]
                return wrapped_data
