# -*- coding: utf-8 -*-
from zope.interface import alsoProvides, implements
from zope.component import adapts
from zope import schema
from plone.directives import form
from plone.dexterity.interfaces import IDexterityContent
from plone.autoform.interfaces import IFormFieldProvider
from zope.schema.vocabulary import SimpleVocabulary, SimpleTerm

from ps.owrsite import _

import logging
logger = logging.getLogger('ps.owrsite')

SECTIONS = SimpleVocabulary([
    SimpleTerm(value=u'allentsteig', title=_(u'Allentsteig')),
    SimpleTerm(value=u'brunn-am-gebirge', title=_(u'Brunn am Gebirge / Mödling')),
    SimpleTerm(value=u'krems-an-der-donau', title=_(u'Krems an der Donau')),
    SimpleTerm(value=u'korneuburg', title=_(u'Korneuburg')),
    SimpleTerm(value=u'landesverband', title=_(u'Landesverband')),
    SimpleTerm(value=u'pottendorf', title=_(u'Pottendorf')),
    SimpleTerm(value=u'st-polten', title=_(u'St. Pölten')),
    SimpleTerm(value=u'traiskirchen', title=_(u'Traiskirchen')),
    SimpleTerm(value=u'tulln', title=_(u'Tulln')),
    SimpleTerm(value=u'waidhofen-an-der-ybbs', title=_(u'Waidhofen an der Ybbs')),
    SimpleTerm(value=u'ybbs-an-der-donau', title=_(u'Ybbs an der Donau'))
])

CATEGORY = SimpleVocabulary([
    SimpleTerm(value=u'veranstaltung', title=_(u'Veranstaltung')),
    SimpleTerm(value=u'kurs', title=_(u'Kurs')),
    SimpleTerm(value=u'weiterbildung-fur-einsatzpersonal', title=_(u'Weiterbildung für Einsatzpersonal')),
    SimpleTerm(value=u'sonstiges', title=_(u'Sonstiges')),
])


def context_property(name):

    def getter(self):
        return getattr(self.context, name)

    def setter(self, value):
        setattr(self.context, name, value)

    def deleter(self):
        delattr(self.context, name)

    return property(getter, setter, deleter)


class ICategorization(form.Schema):
    """"""
    sections = schema.Choice(
        title=_(u'label_sections', default=u'Sections'),
        description=_(u'', default=u''),
        vocabulary=SECTIONS
    )
    category = schema.Choice(
        title=_(u'label_category', default=u'Category'),
        description=_(u'', default=u''),
        vocabulary=CATEGORY
    )

alsoProvides(ICategorization, IFormFieldProvider)


class Categorization(object):
    """
    Factory to store data in attributes
    """
    implements(ICategorization)
    adapts(IDexterityContent)

    def __init__(self, context):
        self.context = context

    sections = context_property('sections')
    category = context_property('category')
