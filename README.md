# Plone Boilerplate

## How to use it

1. Clone or download this repository
2. Run build.sh file and follow the instructions.

  ```
  $ ./build.sh
  ```

3. Run the bootstrap.py file with the development.cfg file.
If you get an error claiming to have a version conflict with setuptools, simply add "-v 2.1.1" at the end.

  ```
  $ python bootstrap.py -c development.cfg
  ```

4. run buildout

  ```
  $ bin/buildout -c development.cfg
  ```


5. Plone is ready, use it - have fun!

  ```
  $ bin/instance fg
  ```
